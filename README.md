# Simple framework to provide converter / populator pattern.
Author: <m.wolff@neusta.de>
v1.0.2, 2021-04-03

## Introduction
For a better seperation of concerns it is good practive to provide view objects which are independent from the business logic. The question is: How to fill. inspired from SAP Commerce Cloud and a video from [Michael Albrecht - only in German](https://www.michael-albrecht.de/converter-populator/) I implmented a converter/populater pattern with some lines of code.

## Idea
The Idea is, you provide Populator Objects which maps some properties from a business data structure into a ViewDTO. A converter collect all populators and all sources and bind them together. Thats all. Actually you can build those converter/populaters per code or witch Spring.

Feel free to use. 

## Integration to maven
```
<dependency>
  <groupId>org.mwolff</groupId>
  <artifactId>simple-converter</artifactId>
  <version>1.0.2</version>
</dependency>
```
For other build tools browse here: https://search.maven.org/artifact/org.mwolff/simple-converter/1.0.2/jar
