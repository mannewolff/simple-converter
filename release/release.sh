if [ $# -eq 0 ]
  then
    echo "No arguments supplied"
    exit 1
fi

echo Release version $1

cp ../target/simple-converter-$1.jar .
cp ../target/simple-converter-$1-javadoc.jar .
cp ../target/simple-converter-$1-sources.jar .
cp ../pom.xml .
rm -rf bundle.jar

gpg -ab pom.xml
gpg -ab simple-converter-$1.jar
gpg -ab simple-converter-$1-javadoc.jar
gpg -ab simple-converter-$1-sources.jar

jar -cfv bundle.jar simple-converter-$1.jar simple-converter-$1.jar.asc simple-converter-$1-javadoc.jar simple-converter-$1-javadoc.jar.asc simple-converter-$1-sources.jar simple-converter-$1-sources.jar.asc pom.xml pom.xml.asc

