package org.mwolff.converter;

/**
 * Converter interface. Converters runs through all added {@link Populator} classes and calls their {@link Populator#convert(Object, ViewDTO)} methods.
 */
public interface Converter {

    /**
     * Adds a {@link Populator} to the list of populators
     * @param populator the {@link Populator} to add.
     */
    Converter addPopulator(final Populator populator);

    /**
     * Adds a source object
     * @param source Source Object
     */
    Converter addSource(final Object source);

    /**
     * Converts the values of sources into the target object.
     * @param viewDTO The target object.
     */
     Converter convert(final ViewDTO viewDTO);

}
