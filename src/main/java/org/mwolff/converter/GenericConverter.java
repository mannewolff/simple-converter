package org.mwolff.converter;

import java.util.ArrayList;
import java.util.List;

/**
 * A generic {@link Converter} to convert with different {@link Populator} classes.
 */
public class GenericConverter implements Converter {

    /**
     *  A list of {@link Populator} classes.
     */
    private List<Populator> populatorlist = new ArrayList<>();

    /**
     * A list of sources to populate.
     */
    private List<Object> sourceList = new ArrayList<>();

    /**
     * For Dependency Injection e.g. Spring
     * @param listToSet A List of Populators.
     */
    public void setPopulatorlist(final List<Populator> listToSet) {
        this.populatorlist = listToSet;
    }

    /**
     * {@inheritDoc}
     * @param populator
     */
    @Override
    public Converter addPopulator(Populator populator) {
        populatorlist.add(populator);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Converter addSource(Object source) {
        sourceList.add(source);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Converter convert(ViewDTO viewDTO) {
        populatorlist.forEach(populator ->
            sourceList.stream().filter(object -> populator.returnType().
                    equals(object.getClass().toString())).
                    forEach(object -> populator.convert(object, viewDTO)));

        return this;
    }
}