package org.mwolff.converter;

import org.springframework.beans.BeanUtils;

/**
 * Generic {@link Populator} for populate properties per name.
 */
public class GenericPopulator implements Populator<Object, ViewDTO> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void convert(Object source, ViewDTO target) {
        BeanUtils.copyProperties(source, target);
    }

    @Override
    public String returnType() {
        return Object.class.toString();
    }

}







