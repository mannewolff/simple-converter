package org.mwolff.converter;

/**
 * Interface for a Populator.
 * @param <S> The source object for the population.
 * @param <T> The dest object for the population.
 */
public interface Populator<S extends Object, T extends ViewDTO> {

    /**
     * Converts the properties from source to target object.
     * @param source Source object.
     * @param target Target object.
     */
    void convert(S source, T target);

    String returnType();
}
