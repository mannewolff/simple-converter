/**
This is a generated class. Please don't change. Changes will be overwritten due next compile cycle.
Generated from Simple Bean Generator 2.0.3
Template is: class-template-hibernate.vm
*/
package org.mwolff;

public class Adresse {

    Adresse getNewInstance() {
        return new Adresse ();
    }

    private Long id;
    private String strasse;
    private String hausnummer;
    private String plz;
    private String ort;

    public String getStrasse() {
        return this.strasse;
    }

    public void setStrasse(final String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return this.hausnummer;
    }

    public void setHausnummer(final String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getPlz() {
        return this.plz;
    }

    public void setPlz(final String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return this.ort;
    }

    public void setOrt(final String ort) {
        this.ort = ort;
    }

}
