/**
This is a generated class. Please don't change. Changes will be overwritten due next compile cycle.
Generated from Simple Bean Generator 2.0.3
Template is: class-template-hibernate.vm
*/
package org.mwolff;

public class Customer {

    Customer getNewInstance() {
        return new Customer ();
    }

    private Long id;
    private String firstname;
    private String lastname;

    public String getFirstname() {
        return this.firstname;
    }

    public void setFirstname(final String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public void setLastname(final String lastname) {
        this.lastname = lastname;
    }

}
