/**
This is a generated class. Please don't change. Changes will be overwritten due next compile cycle.
Generated from Simple Bean Generator 2.0.3
Template is: class-template.vm
*/
package org.mwolff;


import org.mwolff.converter.ViewDTO;

public class UserData implements ViewDTO {

    // Factory Method
    UserData getNewInstance() {
        return new UserData ();
    }

    // Properties
    private String prename;
    private String name;
    private String strasse;
    private String hausnummer;
    private String plz;
    private String ort;

    // Get- / Set operations
    public String getPrename() {
        return this.prename;
    }

    public void setPrename(final String prename) {
        this.prename = prename;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getStrasse() {
        return this.strasse;
    }

    public void setStrasse(final String strasse) {
        this.strasse = strasse;
    }

    public String getHausnummer() {
        return this.hausnummer;
    }

    public void setHausnummer(final String hausnummer) {
        this.hausnummer = hausnummer;
    }

    public String getPlz() {
        return this.plz;
    }

    public void setPlz(final String plz) {
        this.plz = plz;
    }

    public String getOrt() {
        return this.ort;
    }

    public void setOrt(final String ort) {
        this.ort = ort;
    }

}
