package org.mwolff.converter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.Adresse;
import org.mwolff.Customer;
import org.mwolff.UserData;
import org.mwolff.populator.AdressPopulator;
import org.mwolff.populator.UserDataPopulator;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.converter.ObjectTestFactory.createAdresse;
import static org.mwolff.converter.ObjectTestFactory.createCustomer;

class AdressPopulatorTest {


    @DisplayName("Test with a valid UserData object.")
    @Test
    void withValidUserData() {

        // given
        Adresse source = createAdresse();
        UserData target = new UserData();

        // when
        new AdressPopulator()
            .convert(source, target);

        // then
        assertThat(target.getPlz(), is("28197"));
        assertThat(target.getOrt(), is("Bremen"));
        assertThat(target.getStrasse(), is("Butjadinger Str"));
        assertThat(target.getHausnummer(), is("34a"));
    }

    @DisplayName("Test with the generic populator")
    @Test
    void withGenericConverter() {

        // given
        Adresse source = createAdresse();
        UserData target = new UserData();

        // when
        new GenericConverter()
                .addPopulator(new AdressPopulator())
                .addSource(source)
                .convert(target);

        // then
        assertThat(target.getPlz(), is("28197"));
        assertThat(target.getOrt(), is("Bremen"));
        assertThat(target.getStrasse(), is("Butjadinger Str"));
        assertThat(target.getHausnummer(), is("34a"));
    }
    @DisplayName("Test of populator.returntype")
    @Test
    void populatorReturnType() {

        // given
        AdressPopulator populator = new AdressPopulator();
        // then
        assertThat(populator.returnType(), is("class org.mwolff.Adresse"));
    }

    @DisplayName("Test with both, Adress and Namedata")
    @Test
    void integration() {

        // given
        Adresse adresse = createAdresse();
        Customer customer = createCustomer();
        Populator pop = new AdressPopulator();
        UserData target = new UserData();

        // when
        new GenericConverter().addPopulator(pop)
            .addPopulator(new UserDataPopulator())
            .addSource(adresse)
            .addSource(customer)
            .convert(target);

        // then
        assertThat(target.getPrename(), is("Manfred"));
        assertThat(target.getName(), is("Wolff"));

        assertThat(target.getPlz(), is("28197"));
        assertThat(target.getOrt(), is("Bremen"));
        assertThat(target.getStrasse(), is("Butjadinger Str"));
        assertThat(target.getHausnummer(), is("34a"));
    }
}