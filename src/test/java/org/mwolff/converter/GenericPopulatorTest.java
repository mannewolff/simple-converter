package org.mwolff.converter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.Adresse;
import org.mwolff.Customer;
import org.mwolff.UserData;
import org.mwolff.populator.AdressPopulator;
import org.mwolff.populator.UserDataPopulator;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.converter.ObjectTestFactory.*;

class GenericPopulatorTest {

    private class Source {
        public String getPropertyEins() {
            return propertyEins;
        }

        public void setPropertyEins(String propertyEins) {
            this.propertyEins = propertyEins;
        }

        public String getPropertyZwei() {
            return propertyZwei;
        }

        public void setPropertyZwei(String propertyZwei) {
            this.propertyZwei = propertyZwei;
        }

        private String propertyEins;
        private String propertyZwei;
    }

    private class Target implements ViewDTO {
        public String getPropertyEins() {
            return propertyEins;
        }

        public void setPropertyEins(String propertyEins) {
            this.propertyEins = propertyEins;
        }

        public String getPropertyZwei() {
            return propertyZwei;
        }

        public void setPropertyZwei(String propertyZwei) {
            this.propertyZwei = propertyZwei;
        }

        private String propertyEins;
        private String propertyZwei;
    }

    @DisplayName("Testing the generic populator.")
    @Test
    void genericPopulatorTest() {
        Source source = new Source();
        source.propertyEins = "Eins";
        source.propertyZwei = "Zwei";

        Target target = new Target();

        GenericPopulator pop = new GenericPopulator();
        pop.convert(source, target);

        assertThat(pop.returnType(), is("class java.lang.Object"));
        assertThat(target.propertyEins, is("Eins"));
    }

    @DisplayName("Test dependency injection point")
    @Test
    void integrationTest() {
        // given
        Adresse adresse = createAdresse();
        Customer customer = createCustomer();
        Populator pop = new AdressPopulator();
        Populator pop2 = new UserDataPopulator();
        UserData userData = new UserData();

        List<Populator> popList = new ArrayList<>();
        popList.add(pop);
        popList.add(pop2);

        // when
        GenericConverter converter = new GenericConverter();
        converter.setPopulatorlist(popList);
        converter.addSource(adresse)
                .addSource(customer)
                .convert(userData);

        // then
        assertThat(userData.getPrename(), is("Manfred"));
        assertThat(userData.getName(), is("Wolff"));

        assertThat(userData.getPlz(), is("28197"));
        assertThat(userData.getOrt(), is("Bremen"));
        assertThat(userData.getStrasse(), is("Butjadinger Str"));
        assertThat(userData.getHausnummer(), is("34a"));
    }
}