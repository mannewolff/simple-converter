package org.mwolff.converter;

import org.mwolff.Adresse;
import org.mwolff.Customer;

public class ObjectTestFactory {

    static public Customer createCustomer() {
        Customer customer = new Customer();
        customer.setFirstname("Manfred");
        customer.setLastname("Wolff");
        return customer;
    }
    static public Adresse createAdresse() {
        Adresse adresse = new Adresse();
        adresse.setPlz("28197");
        adresse.setOrt("Bremen");
        adresse.setStrasse("Butjadinger Str");
        adresse.setHausnummer("34a");
        return adresse;
    }

}
