package org.mwolff.converter;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mwolff.Adresse;
import org.mwolff.Customer;
import org.mwolff.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.converter.ObjectTestFactory.createAdresse;
import static org.mwolff.converter.ObjectTestFactory.createCustomer;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application.xml")
public class SpringPopulatorTest {

    @Autowired
    GenericConverter userdataconverter;

    @Test
    public void testConverterNotNull() {
        assertThat(userdataconverter, notNullValue());
        List<Populator> populatorList = (List<Populator>) ReflectionTestUtils.getField(userdataconverter, "populatorlist");
        assertThat(populatorList, notNullValue());
    }

    @DisplayName("Testing generic converter configures with Spring")
    @Test
    public void populateAll() {

        // given
        Adresse adresse = createAdresse();
        Customer customer = createCustomer();
        UserData target = new UserData();

        // when
        userdataconverter.addSource(adresse)
        .addSource(customer)
        .convert(target);

        // then
        assertThat(target.getPrename(), is("Manfred"));
        assertThat(target.getName(), is("Wolff"));

        assertThat(target.getPlz(), is("28197"));
        assertThat(target.getOrt(), is("Bremen"));
        assertThat(target.getStrasse(), is("Butjadinger Str"));
        assertThat(target.getHausnummer(), is("34a"));
    }

}
