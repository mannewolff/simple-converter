package org.mwolff.converter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mwolff.Customer;
import org.mwolff.UserData;
import org.mwolff.populator.UserDataPopulator;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mwolff.converter.ObjectTestFactory.createCustomer;

class UserDataPopulatorTest {

    @DisplayName("Test with a valid UserData object.")
    @Test
    void withValidUserData() {

        // given
        Customer source = createCustomer();
        UserData target = new UserData();

        // when
        new UserDataPopulator()
            .convert(source, target);

        // then
        assertThat(target.getPrename(), is("Manfred"));
        assertThat(target.getName(), is("Wolff"));
    }

    @DisplayName("Test with the generic populator")
    @Test
    void withGenericConverter() {

        // given
        Customer source = createCustomer();
        UserData target = new UserData();

        // when
        new GenericConverter()
            .addPopulator(new UserDataPopulator())
            .addSource(source)
            .convert(target);

        // then
        assertThat(target.getPrename(), is("Manfred"));
        assertThat(target.getName(), is("Wolff"));
    }

}