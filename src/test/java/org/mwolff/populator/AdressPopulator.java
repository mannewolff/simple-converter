package org.mwolff.populator;

import org.mwolff.Adresse;
import org.mwolff.UserData;
import org.mwolff.converter.Populator;

public class AdressPopulator implements Populator<Adresse, UserData> {

    @Override
    public void convert(Adresse source, UserData target) {
            target.setStrasse(source.getStrasse());
            target.setPlz(source.getPlz());
            target.setOrt(source.getOrt());
            target.setHausnummer(source.getHausnummer());
    }

    @Override
    public String returnType() {
        return Adresse.class.toString();
    }


}