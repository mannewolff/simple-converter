package org.mwolff.populator;

import org.mwolff.Customer;
import org.mwolff.UserData;
import org.mwolff.converter.Populator;

public class UserDataPopulator implements Populator<Customer, UserData> {

    @Override
    public void convert(Customer source, UserData target) {
            target.setName(source.getLastname());
            target.setPrename(source.getFirstname());
    }

    @Override
    public String returnType() {
        return Customer.class.toString();
    }

}